# Site-preloader

Simple, ready site preloader for loading inner data like in "background"

## Use 1
insert html, css, js to your project:
```html
<head>
	...
	<link rel="stylesheet" href="/*path_to_site_preloader_css_file*/">
	...
</head>
<body>
	/*insert site_preloader_html to the top of document*/
	...
	<script src="/*path_to_site_preloader_js_file*/"></script>
	...
</body>
```
then, when your content will be load, call:
```js
SitePreloader.hide();
```

## Use 2
insert css and js to your project:
```html
<head>
	...
	<link rel="stylesheet" href="/*path_to_site_preloader_css_file*/">
	...
</head>
<body>
	...
	<script src="/*path_to_site_preloader_js_file*/"></script>
	...
</body>
```
then, to insert site-preloader html to the top of document, call:
```js
SitePreloader.init();
```
then, when your content will be load, call:
```js
SitePreloader.hide();
```

## hide(options)

* ('opacity'|'up'|'down'): string
* ({}): object
  * type = 'opacity'|'up'|'down': string
  * duration = 1: number 
  * timing_function = 'easy': string
  * delay = 0: number