'use strict';

let fs = require('fs');
let path = require('path');

let gulp = require('gulp');
let gDebug = require('gulp-debug');
let gIf = require('gulp-if');
let gPug = require('gulp-pug');
let gRename = require('gulp-rename');
let gBabel = require('gulp-babel');
let gUglify = require('gulp-uglify');
let gCleanCss = require('gulp-clean-css');
let gStylus = require('gulp-stylus');
let gAutoprefixer = require('gulp-autoprefixer');

let devmode = 0;
let watch = 0;
let argv = process.argv;

// PATHS
const PATH_TO_SRC_DIR = path.join(__dirname,'src');
const PATH_TO_PUBLIC_DIR = path.join(__dirname,'public');

const PATH_TO_SRC_PUG_FILES = path.join(PATH_TO_SRC_DIR,'views','**','*.pug');
const PATH_TO_SRC_STYLUS_FILES = path.join(PATH_TO_SRC_DIR,'stylesheets','**','*.styl');
const PATH_TO_SRC_JS_FILES = path.join(PATH_TO_SRC_DIR,'js','**','*.js');

const PATH_TO_PUBLIC_CSS_DIR = path.join(PATH_TO_PUBLIC_DIR,'css');
const PATH_TO_PUBLIC_JS_DIR = path.join(PATH_TO_PUBLIC_DIR,'js');
const PATH_TO_PUBLIC_FONTS_DIR = path.join(PATH_TO_PUBLIC_DIR,'fonts');


// TASKS
gulp.task('pug_2_html', pug_2_html);
gulp.task('stylus_2_css', stylus_2_css);
gulp.task('compile_js', compile_js);

gulp.task('watch_pug', watch_pug);
gulp.task('watch_stylus', watch_stylus);
gulp.task('watch_js', watch_js);

gulp.task('build', [
	'pug_2_html',
	'stylus_2_css','compile_js'
], () => {
	if (!watch) console.log('Build done success');
});
gulp.task('watch', [
	'watch_pug','watch_stylus','watch_js'
], () => {});

gulp.task('default', ['build','watch']);
gulp.task('init', ['build']);


// BUILD
function pug_2_html() {
	return gulp.src(PATH_TO_SRC_PUG_FILES)
			.pipe(gPug({
				pretty: devmode,
			}))
			.pipe(gulp.dest(PATH_TO_PUBLIC_DIR));
}
function stylus_2_css() {
	return gulp.src(PATH_TO_SRC_STYLUS_FILES)
			.pipe(gStylus())
			.pipe(gAutoprefixer())
			.pipe(gulp.dest(PATH_TO_PUBLIC_CSS_DIR))
			.pipe(gIf(!devmode, gCleanCss()))
			.pipe(gIf(!devmode, gRename(renameAddMinExt)))
			.pipe(gIf(!devmode, gulp.dest(PATH_TO_PUBLIC_CSS_DIR)))
}
function compile_js() {
	return gulp.src(PATH_TO_SRC_JS_FILES)
			.pipe(gBabel({
				presets: ['es2015']
			}))
			.pipe(gulp.dest(PATH_TO_PUBLIC_JS_DIR))
			.pipe(gIf(!devmode, gUglify()))
			.pipe(gIf(!devmode, gRename(renameAddMinExt)))
			.pipe(gIf(!devmode, gulp.dest(PATH_TO_PUBLIC_JS_DIR)));
}


// WATCH
function watchWatcher(watcher, title) {
	watcher.on('change', (event) => {
		console.log(`${title} "${event.type}" in path: ${event.path}`);
	});
}

function watch_pug() {
	let watcher = gulp.watch(PATH_TO_SRC_PUG_FILES,['pug_2_html']);
	watchWatcher(watcher,'Watch pug');
}
function watch_stylus() {
	let watcher = gulp.watch(PATH_TO_SRC_STYLUS_FILES,['stylus_2_css']);
	watchWatcher(watcher,'Watch stylus');
}
function watch_js() {
	let watcher = gulp.watch(PATH_TO_SRC_JS_FILES,['compile_js']);
	watchWatcher(watcher,'Watch js');
}


// ANOTHER
function rmDir(dirPath, removeSelf) {
	if (removeSelf === undefined)
		removeSelf = true;
	try { var files = fs.readdirSync(dirPath); }
	catch(e) { return; }
	if (files.length > 0)
		for (var i = 0; i < files.length; i++) {
			var filePath = dirPath + '/' + files[i];
			if (fs.statSync(filePath).isFile())
				fs.unlinkSync(filePath);
			else
				rmDir(filePath);
		}
	if (removeSelf)
		fs.rmdirSync(dirPath);
}

function renameAddMinExt(path) {
	path.basename += '.min';
	return path;
}

// FOR npm run
if (argv.length > 2) {
	if (argv[2] === 'development') {
		devmode = 1;
	}
	if (argv.length > 3) watch = 1;
}
console.log("MODE", devmode ? 'development' : 'production');
if (watch) console.log('with watch changes');

rmDir('public');
console.log('All previous public files success removed');

if (watch) {
	console.log('Start watch src directory');
	gulp.start('default');
} else {
	console.log('Please, wait for compile project');
	gulp.start('init');
}