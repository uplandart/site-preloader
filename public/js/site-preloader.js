'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

(function () {
	'use strict';

	if (window.SitePreloader) {
		console.error('SitePreloader already load');
		return;
	}

	var _el_site_preloader = document.getElementById('site_preloader');

	function elStyleDisplaySetNoneEvent() {
		_el_site_preloader.style.display = "none";
	}

	function setTransitionOptions(duration, timing_function, delay) {
		_el_site_preloader.addEventListener('webkitTransitionEnd', elStyleDisplaySetNoneEvent, false);
		_el_site_preloader.addEventListener('mozTransitionEnd', elStyleDisplaySetNoneEvent, false);
		_el_site_preloader.addEventListener('oTransitionEnd', elStyleDisplaySetNoneEvent, false);
		_el_site_preloader.addEventListener('transitionend', elStyleDisplaySetNoneEvent, false);
		_el_site_preloader.style.webkitTransitionDuration = duration;
		_el_site_preloader.style.mozTransitionDuration = duration;
		_el_site_preloader.style.oTransitionDuration = duration;
		_el_site_preloader.style.transitionDuration = duration;
		_el_site_preloader.style.webkitTransitionTimingFunction = timing_function;
		_el_site_preloader.style.mozTransitionTimingFunction = timing_function;
		_el_site_preloader.style.oTransitionTimingFunction = timing_function;
		_el_site_preloader.style.transitionTimingFunction = timing_function;
		_el_site_preloader.style.webkitTransitionDelay = delay;
		_el_site_preloader.style.mozTransitionDelay = delay;
		_el_site_preloader.style.oTransitionDelay = delay;
		_el_site_preloader.style.transitionDelay = delay;
	}

	function hide(args) {
		var opts = { type: 'opacity', duration: 1, timing_function: 'easy', delay: 0 };
		if (typeof args === 'string') {
			opts.type = args;
		} else if ((typeof args === 'undefined' ? 'undefined' : _typeof(args)) === 'object') {
			opts = Object.assign(opts, args);
		}
		setTransitionOptions(opts.duration + 's', opts.timing_function, opts.delay + 's');
		switch (opts.type) {
			case 'up':
				_el_site_preloader.style.bottom = window.innerHeight + 'px';
				break;
			case 'down':
				_el_site_preloader.style.top = window.innerHeight + 'px';
				break;
			case 'opacity':
			default:
				_el_site_preloader.style.opacity = "0";
				break;
		}
	}

	function init() {
		if (_el_site_preloader) {
			console.error('_el_site_preloader already defined');
			return;
		}
		_el_site_preloader = document.createElement('DIV');
		_el_site_preloader.id = 'site_preloader';
		_el_site_preloader.innerHTML = '\n<div class="loader">\n\t<div class="cell"></div>\n\t<div class="cell"></div>\n\t<div class="cell"></div>\n\t<div class="cell"></div>\n</div>';
		document.body.insertBefore(_el_site_preloader, document.body.firstChild);
	}

	window.SitePreloader = {
		hide: hide,
		init: init
	};
})();